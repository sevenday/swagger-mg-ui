# swagger-mg-ui

#### 项目介绍
swagger-mg-ui是swagger-ui的一个前端实现，使用简单、解析速度快、走心的设计，支持多项目同时展示，多种文档目录的展示方案，多种自定义配置，满足各种使用习惯，使用中您有任何的意见和建议都可到源码地址处反馈哦！

每一行代码都是从头开始写的，所以每一个问题都能及时得到解决
欢迎 Fork、Star、Issues、、、

demo代码地址：
[swagger-mg-ui-demo](https://gitee.com/zyplayer/swagger-mg-ui-demo)

已上传至中央仓库，使用方法：
```
<!-- https://mvnrepository.com/artifact/com.zyplayer/swagger-mg-ui -->
<dependency>
    <groupId>com.zyplayer</groupId>
    <artifactId>swagger-mg-ui</artifactId>
    <version>1.0.1</version>
</dependency>
```

#### 服务器端的存储能力功能：
实现MgStorageService并申明为@Service之后网页上才能使用服务器端的存储能力，同时需要在@EnableSwagger2的地方添加@EnableSwaggerMgUi注解，才能开启存储的接口
开放存储能力的好处：
所有网页的配置、调试值都可以存储到服务器的数据库中，便于团队所有人的调试，一人配置，所有人受益
如果不开启的话，数据是存放在浏览器的localStorage中，每个人、每个浏览器都得配置一次才能使用

#### MgUiTestTestFilter.java 功能：
判断是否是模拟请求，功能需求：
很多时候后端定义好了接口，但还未实现，这时前端已经需要数据调试了，这时就需要用到这个过滤器了！
在页面上先配置好模拟返回的数据，然后在url上加入参数：mgUiTestFlag=1
例：http://192.168.0.249:8082/openApi/case/info?mgUiTestFlag=1
本过滤器就直接返回了之前配置的模拟数据，而不用等到后端必须把接口实现之后才能调试，或者在前端写一大段测试数据。

例：笔者的公司后端人较少，一个需求需要10个接口，需求分析完后首先就把接口、参数、返回值定义好，然后一个个的去实现。
也许需要10天才能写完，但前端两天就写好了，急需数据看效果，这时就让他们自己去设置模拟值，加上参数自己测试好。
而不是一味的催后台，把各种锅丢给后端，然后玩自己的去了，浪费各环节等待时间。

#### ↑↑以上个性化功能在demo中都有写到，欢迎体验↑↑：

#### 功能细节：
1. 支持添加多个文档，同时展示
2. 左侧的侧边栏支持左右拖动改变大小
3. 优化各种细节，做到能不展示就不展示
4. 服务器端的存储能力
5. 模拟请求过滤能力
6. 请求参数缓存能力
7. 更多细节等你发现

文档展示页面：
![基本界面](https://images.gitee.com/uploads/images/2018/0810/130330_2afbe471_596905.jpeg "111.jpg")
在线调试页面：
![在线调试页面](https://images.gitee.com/uploads/images/2018/0810/130423_ab28bc6f_596905.jpeg "22.jpg")

如果需要看多个项目的文档，需要对让被访问的项目支持跨域访问
```
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
@Component
@WebFilter(urlPatterns = { "/*" })
public class CaptureFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {}

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        String requestURI = httpServletRequest.getRequestURI();
        if (requestURI.endsWith("/swagger-resources") || requestURI.endsWith("/v2/api-docs")) {
            httpServletResponse.addHeader("Access-Control-Allow-Origin", "*");
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {}

}
```


#### 项目优势
首创按路径一级一级的分开展示，同时支持按Tag的方式浏览目录，优化文档的展示，主页支持目录展示方式的切换，支持多个项目同时展示

#### 软件架构
maven项目，代码是html、js、css组成的

前端框架使用的[zui](http://zui.sexy)

