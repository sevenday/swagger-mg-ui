package com.mg.swagger.controller;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.mg.swagger.framework.constant.StorageKeys;
import com.mg.swagger.framework.constant.Toast;
import com.mg.swagger.framework.json.MgUiResponseJson;
import com.mg.swagger.framework.json.ResponseJson;
import com.mg.swagger.framework.service.MgStorageService;

import cn.hutool.http.HttpRequest;
import springfox.documentation.swagger.web.SwaggerResource;

/**
 * 文档控制器
 * 
 * @author 暮光：城中城
 * @since 2018年8月21日
 */
@RestController
@RequestMapping("/swagger-mg-ui/document")
public class DocumentController {

	@Autowired(required = false)
	private MgStorageService storageService;

	/**
	 * 获取所有的文档
	 * 
	 * @author 暮光：城中城
	 * @since 2018年8月21日
	 * @return
	 */
	@PostMapping(value = "/docs")
	public ResponseJson docs(HttpServletRequest request) {
		if (storageService == null) {
			return MgUiResponseJson.warn(Toast.AUTOWIRED_ERROR);
		}
		String swaggerResourcesStr = storageService.get(StorageKeys.SWAGGER_RESOURCES_LIST);
		String swaggerDocsDeleteStr = storageService.get(StorageKeys.SWAGGER_DOCS_DELETE_LIST);
		// 转成set，防止重复
		Set<String> resourcesSet = new HashSet<>();
		if (StringUtils.isNotBlank(swaggerResourcesStr)) {
			List<String> resourcesList = JSON.parseArray(swaggerResourcesStr, String.class);
			resourcesSet.addAll(resourcesList);
		} else {
			// 默认加上自身的文档
			String serverPath = request.getContextPath() + request.getServletPath();
			resourcesSet.add(serverPath + "/swagger-resources");
		}
		Set<String> swaggerDocsDeleteSet = new HashSet<>();
		if (StringUtils.isNotBlank(swaggerDocsDeleteStr)) {
			List<String> swaggerDocsDeleteList = JSON.parseArray(swaggerDocsDeleteStr, String.class);
			swaggerDocsDeleteSet.addAll(swaggerDocsDeleteList);
		}
		List<JSONObject> swaggerResourceList = new LinkedList<>();
		for (String resourcesUrl : resourcesSet) {
			List<SwaggerResource> resourceList = null;
			try {
				String resourcesStr = HttpRequest.get(resourcesUrl).execute().body();
				resourceList = JSON.parseArray(resourcesStr, SwaggerResource.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (resourceList == null || resourceList.isEmpty()) {
				continue;
			}
			resourcesUrl = resourcesUrl.substring(0, resourcesUrl.lastIndexOf("/") + 1);
			for (SwaggerResource resource : resourceList) {
				// 已删除的则不处理
				if (swaggerDocsDeleteSet.contains(resource.getLocation())) {
					continue;
				}
				JSONObject jsonObject = null;
				try {
					String location = resourcesUrl + resource.getLocation();
					String resourceStr = HttpRequest.get(location).execute().body();
					jsonObject = JSON.parseObject(resourceStr);
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (jsonObject == null || jsonObject.isEmpty()) {
					continue;
				}
				swaggerResourceList.add(jsonObject);
			}
		}
		return MgUiResponseJson.ok(swaggerResourceList);
	}

	/**
	 * 增加/swagger-resources地址
	 * 
	 * @author 暮光：城中城
	 * @since 2018年8月21日
	 * @param resourcesUrl
	 * @return
	 */
	@PostMapping(value = "/addSwaggerResources")
	public ResponseJson addSwaggerResources(String resourcesUrl) {
		if (storageService == null) {
			return MgUiResponseJson.warn(Toast.AUTOWIRED_ERROR);
		}
		String swaggerResourcesStr = storageService.get(StorageKeys.SWAGGER_RESOURCES_LIST);
		String swaggerDocsDeleteStr = storageService.get(StorageKeys.SWAGGER_DOCS_DELETE_LIST);
		Set<String> swaggerDocsDeleteSet = new HashSet<>();
		if (StringUtils.isNotBlank(swaggerDocsDeleteStr)) {
			List<String> swaggerDocsDeleteList = JSON.parseArray(swaggerDocsDeleteStr, String.class);
			swaggerDocsDeleteSet.addAll(swaggerDocsDeleteList);
		}
		// 转成set，防止重复
		Set<String> resourcesSet = new HashSet<>();
		if (StringUtils.isNotBlank(swaggerResourcesStr)) {
			List<String> resourcesList = JSON.parseArray(swaggerResourcesStr, String.class);
			resourcesSet.addAll(resourcesList);
		}
		try {
			String resourcesStr = HttpRequest.get(resourcesUrl).execute().body();
			List<SwaggerResource> resourceList = JSON.parseArray(resourcesStr, SwaggerResource.class);
			if (resourceList == null || resourceList.isEmpty()) {
				return MgUiResponseJson.warn("改地址未找到文档");
			}
			// 重新加入的时候把之前的已删除的回恢复
			for (SwaggerResource swaggerResource : resourceList) {
				swaggerDocsDeleteSet.remove(swaggerResource.getLocation());
			}
			resourcesSet.add(resourcesUrl);
		} catch (Exception e) {
			e.printStackTrace();
			return MgUiResponseJson.warn("改地址查找文档失败");
		}
		storageService.put(StorageKeys.SWAGGER_RESOURCES_LIST, JSON.toJSONString(resourcesSet));
		storageService.put(StorageKeys.SWAGGER_DOCS_DELETE_LIST, JSON.toJSONString(swaggerDocsDeleteSet));
		return MgUiResponseJson.ok();
	}

	/**
	 * 删除/v2/api-docs
	 * 
	 * @author 暮光：城中城
	 * @since 2018年8月21日
	 * @param resourcesUrl
	 * @return
	 */
	@PostMapping(value = "/deleteSwaggerDoc")
	public ResponseJson deleteSwaggerDoc(String docUrl) {
		if (storageService == null) {
			return MgUiResponseJson.warn(Toast.AUTOWIRED_ERROR);
		}
		String swaggerDocsDeleteStr = storageService.get(StorageKeys.SWAGGER_DOCS_DELETE_LIST);
		Set<String> swaggerDocsDeleteSet = new HashSet<>();
		if (StringUtils.isNotBlank(swaggerDocsDeleteStr)) {
			List<String> swaggerDocsDeleteList = JSON.parseArray(swaggerDocsDeleteStr, String.class);
			swaggerDocsDeleteSet.addAll(swaggerDocsDeleteList);
		}
		swaggerDocsDeleteSet.add(docUrl);
		storageService.put(StorageKeys.SWAGGER_DOCS_DELETE_LIST, JSON.toJSONString(swaggerDocsDeleteSet));
		return MgUiResponseJson.ok();
	}
}
