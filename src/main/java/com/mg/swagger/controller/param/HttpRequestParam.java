package com.mg.swagger.controller.param;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.Method;

/**
 * 请求参数对象
 * @author 暮光：城中城
 * @since 2018年8月21日
 */
public class HttpRequestParam {
	private String url;
	private String header;
	private String form;
	private String body;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Map<String, String> getHeaderMap() {
		if (StringUtils.isBlank(header)) {
			return null;
		}
		Map<String, String> headerMap = JSON.parseObject(header, new TypeReference<HashMap<String, String>>() {
		});
		return headerMap;
	}

	public Map<String, Object> getFormMap() {
		if (StringUtils.isBlank(form)) {
			return null;
		}
		Map<String, Object> formMap = JSON.parseObject(form, new TypeReference<HashMap<String, Object>>() {
		});
		return formMap;
	}

	public void createHttpRequest(HttpRequest request) {
		Map<String, String> headerMap = this.getHeaderMap();
		if (headerMap != null) {
			request.addHeaders(headerMap);
		}
		Map<String, Object> formMap = this.getFormMap();
		if (formMap != null) {
			request.form(formMap);
		}
		if (StringUtils.isNotBlank(body) && request.getMethod() != Method.GET) {
			request.body(body);
		}
	}

	/**
	 * POST请求
	 */
	public HttpRequest post() {
		HttpRequest request = HttpRequest.post(this.getUrl());
		this.createHttpRequest(request);
		return request;
	}

	/**
	 * GET请求
	 */
	public HttpRequest get() {
		HttpRequest request = HttpRequest.get(this.getUrl());
		this.createHttpRequest(request);
		return request;
	}

	/**
	 * HEAD请求
	 */
	public HttpRequest head() {
		HttpRequest request = HttpRequest.head(this.getUrl());
		this.createHttpRequest(request);
		return request;
	}

	/**
	 * OPTIONS请求
	 */
	public HttpRequest options() {
		HttpRequest request = HttpRequest.options(this.getUrl());
		this.createHttpRequest(request);
		return request;
	}

	/**
	 * PUT请求
	 */
	public HttpRequest put() {
		HttpRequest request = HttpRequest.put(this.getUrl());
		this.createHttpRequest(request);
		return request;
	}

	/**
	 * PATCH请求
	 */
	public HttpRequest patch() {
		HttpRequest request = HttpRequest.patch(this.getUrl());
		this.createHttpRequest(request);
		return request;
	}

	/**
	 * DELETE请求
	 */
	public HttpRequest delete() {
		HttpRequest request = HttpRequest.delete(this.getUrl());
		this.createHttpRequest(request);
		return request;
	}

	/**
	 * TRACE请求
	 */
	public HttpRequest trace() {
		HttpRequest request = HttpRequest.trace(this.getUrl());
		this.createHttpRequest(request);
		return request;
	}

}
