package com.mg.swagger.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mg.swagger.controller.param.HttpRequestParam;
import com.mg.swagger.framework.json.MgUiResponseJson;
import com.mg.swagger.framework.json.ResponseJson;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;

/**
 * 后台代理网络请求的控制器
 * 
 * @author 暮光：城中城
 * @since 2018年8月21日
 */
@RestController
@RequestMapping("/swagger-mg-ui/http")
public class HttpRequestController {

	@RequestMapping(value = "/request", method = RequestMethod.GET)
	public ResponseJson get(HttpRequestParam param) {
		HttpRequest request = param.get();
		HttpResponse execute = request.execute();
		return MgUiResponseJson.ok(execute);
	}

	@RequestMapping(value = "/request", method = RequestMethod.POST)
	public ResponseJson post(HttpRequestParam param) {
		HttpRequest request = param.post();
		HttpResponse execute = request.execute();
		return MgUiResponseJson.ok(execute);
	}

	@RequestMapping(value = "/request", method = RequestMethod.HEAD)
	public ResponseJson head(HttpRequestParam param) {
		HttpRequest request = param.head();
		HttpResponse execute = request.execute();
		return MgUiResponseJson.ok(execute);
	}

	@RequestMapping(value = "/request", method = RequestMethod.OPTIONS)
	public ResponseJson options(HttpRequestParam param) {
		HttpRequest request = param.options();
		HttpResponse execute = request.execute();
		return MgUiResponseJson.ok(execute);
	}

	@RequestMapping(value = "/request", method = RequestMethod.PUT)
	public ResponseJson put(HttpRequestParam param) {
		HttpRequest request = param.put();
		HttpResponse execute = request.execute();
		return MgUiResponseJson.ok(execute);
	}

	@RequestMapping(value = "/request", method = RequestMethod.DELETE)
	public ResponseJson delete(HttpRequestParam param) {
		HttpRequest request = param.delete();
		HttpResponse execute = request.execute();
		return MgUiResponseJson.ok(execute);
	}

	@RequestMapping(value = "/request", method = RequestMethod.TRACE)
	public ResponseJson trace(HttpRequestParam param) {
		HttpRequest request = param.trace();
		HttpResponse execute = request.execute();
		return MgUiResponseJson.ok(execute);
	}

	@RequestMapping(value = "/request", method = RequestMethod.PATCH)
	public ResponseJson patch(HttpRequestParam param) {
		HttpRequest request = param.patch();
		HttpResponse execute = request.execute();
		return MgUiResponseJson.ok(execute);
	}
}
