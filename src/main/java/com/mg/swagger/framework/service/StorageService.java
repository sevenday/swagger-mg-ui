package com.mg.swagger.framework.service;

/**
 * 存储服务器接口
 * @author 暮光：城中城
 * @since 2018年8月21日
 */
public interface StorageService {
	
	/**
	 * 获取存储的值
	 * @author 暮光：城中城
	 * @since 2018年8月21日
	 * @param name
	 * @return
	 */
	String get(String name);
	
	/**
	 * 设置存储值
	 * @author 暮光：城中城
	 * @since 2018年8月21日
	 * @param name
	 * @param value
	 */
	void put(String name, String value);
	
}