package com.mg.swagger.framework.configuration;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration
@ComponentScan(basePackages = {
	"com.mg.swagger.controller",
	"com.mg.swagger.framework.service",
})
public class SwaggerCommonConfiguration {

}
